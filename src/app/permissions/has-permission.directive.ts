import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { PermissionService } from './permission.service';
import { Permission } from './permission.enum';

@Directive({
  selector: '[hasPermission]',
})
export class HasPermissionDirective {
  private elseTemplateRef?: TemplateRef<unknown>;
  private hasCurrentPermission = false;

  constructor(
    private readonly viewContainer: ViewContainerRef,
    private readonly templateRef: TemplateRef<unknown>,
    private readonly permissionService: PermissionService
  ) {
  }

  @Input() set hasPermissionElse(templateRef: TemplateRef<unknown>) {
    this.elseTemplateRef = templateRef;
    this.displayTemplate();
  }

  @Input() set hasPermission(permission: Permission) {
    this.hasCurrentPermission = this.permissionService.hasPermission(permission);
    this.displayTemplate();
  }

  private displayTemplate(): void {
    this.viewContainer.clear();

    if (this.hasCurrentPermission) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      return;
    }

    if (this.elseTemplateRef) {
      this.viewContainer.createEmbeddedView(this.elseTemplateRef);
    }
  }
}
