export enum Permission {
  login = 'login',
  dashboard = 'dashboard',
  orders = 'orders',
  users = 'users',
  admin = 'admin',
}
