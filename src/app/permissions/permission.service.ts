import { Injectable } from '@angular/core';
import { Permission } from './permission.enum';

@Injectable()
export class PermissionService {
  constructor(private readonly permissions: Permission[]) {
  }

  hasPermission(permission: Permission): boolean {
    return this.permissions.includes(permission);
  }
}
