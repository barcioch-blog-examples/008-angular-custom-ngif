import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { Permission } from './permission.enum';
import { CommonModule } from '@angular/common';
import { HasPermissionModule } from './has-permission.module';
import { PermissionService } from './permission.service';
import { By } from '@angular/platform-browser';

describe('HasPermissionDirective', () => {
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        HasPermissionModule
      ],
      declarations: [TestComponent],
      providers: [
        {
          provide: PermissionService,
          useValue: new PermissionService([Permission.dashboard, Permission.users])
        }
      ]
    });

    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges()
  });

  describe('when the test component is initialized', () => {
    describe('and user has set "dashboard" and "users" permissions', () => {
      it('should not display "orders" element', () => {
        const element = fixture.debugElement.query(By.css('#orders'));
        expect(element).toBeFalsy();
      });

      it('should display "dashboard" element', () => {
        const element = fixture.debugElement.query(By.css('#dashboard'));
        expect(element).toBeTruthy();
      });

      it('should not display "admin" element', () => {
        const element = fixture.debugElement.query(By.css('#admin'));
        expect(element).toBeFalsy();
      });

      it('should display "no-access-admin" element', () => {
        const element = fixture.debugElement.query(By.css('#no-access-admin'));
        expect(element).toBeTruthy();
      });

      it('should display "no-access-dashboard" element', () => {
        const element = fixture.debugElement.query(By.css('#admin'));
        expect(element).toBeFalsy();
      });


      it('should display "users" element', () => {
        const element = fixture.debugElement.query(By.css('#users'));
        expect(element).toBeTruthy();
      });

      it('should display "no-access-users" element', () => {
        const element = fixture.debugElement.query(By.css('#no-access-users'));
        expect(element).toBeFalsy();
      });

    });
  });
});


@Component({
  selector: 'app-test',
  template: `
    <div id="orders" *hasPermission="permissions.orders"></div>

    <div id="dashboard" *hasPermission="permissions.dashboard"></div>

    <div id="admin" *hasPermission="permissions.admin; else noAccessAdmin"></div>

    <ng-template #noAccessAdmin>
      <div id="no-access-admin"></div>
    </ng-template>

    <div id="users" *hasPermission="permissions.users; else noAccessUsers"></div>

    <ng-template #noAccessUsers>
      <div id="no-access-users"></div>
    </ng-template>
  `,
})
export class TestComponent {
  readonly permissions = Permission;
}
